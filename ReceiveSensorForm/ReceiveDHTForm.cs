﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CYFang
{
    public partial class ReceiveDHTForm : Form
    {
        /// <summary>
        /// COM Port
        /// </summary>
        private static SerialPort comPort;

        /// <summary>
        /// 濕度線段
        /// </summary>
        private static Series seriesH = new Series("濕度", 100);

        /// <summary>
        /// 溫度線段
        /// </summary>
        private static Series seriesT = new Series("溫度", 100);

        /// <summary>
        /// Worker
        /// </summary>
        private static BackgroundWorker worker = new BackgroundWorker();

        /// <summary>
        /// 字串陣列
        /// </summary>
        private static readonly String[] INFOS = new String[] {
            "目前濕度為：{0}%",
            "目前溫度為：{0}°C"
        };

        /// <summary>
        /// Baud Rate
        /// </summary>
        private static readonly int BAUD_RATE = 9600;

        /// <summary>
        /// Count
        /// </summary>
        private static long count = 0;

        /// <summary>
        /// Humidity
        /// </summary>
        private static double totalH = 0.0d;

        /// <summary>
        /// Temperature
        /// </summary>
        private static double totalT = 0.0d;

        /// <summary>
        /// Mutex
        /// </summary>
        private Mutex _mutex;

        /// <summary>
        /// Construct
        /// </summary>
        public ReceiveDHTForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Form Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReceiveDHTForm_Load(object sender, EventArgs e)
        {
            var port = SerialPort.GetPortNames().Select(p => p).Where(p => p.Equals("COM3")).FirstOrDefault();
            if (string.IsNullOrEmpty(port) || port == null)
            {
                MessageBox.Show("請將您的Arduino與電腦連接", "將為你重啟應用程式", MessageBoxButtons.OK);
                Application.Restart();
            }
            else
            {
                //設定COM Port
                comPort = new SerialPort(port, BAUD_RATE);
                //讀取逾時時間
                comPort.ReadTimeout = 1500;
                //讀取緩衝空間
                comPort.ReadBufferSize = 150;

                //視窗關閉事件
                Application.ApplicationExit += (s, ea) =>
                {
                    if (this._mutex != null)
                    {
                        this._mutex.ReleaseMutex();
                        this._mutex.Close();
                    }
                };

                //允許worker支援非同步取消
                worker.WorkerSupportsCancellation = true;
                //允許worker回報進度
                worker.WorkerReportsProgress = true;

                //worker工作事件
                worker.DoWork += (s, ea) =>
                {
                    //取消工作時
                    if (worker.CancellationPending)
                    {
                        ea.Cancel = true;
                        return;
                    }
                    else
                    {
                        while (!worker.CancellationPending && comPort.IsOpen)
                        {
                            try
                            {
                                var str = comPort.ReadLine();
                                worker.ReportProgress(0, str);
                                Thread.Sleep(1000);
                            }
                            catch (TimeoutException ex)
                            {
                                Console.WriteLine(ex.StackTrace);
                            }
                            catch (IOException ex)
                            {
                                Console.WriteLine(ex.StackTrace);
                            }
                        }
                    }
                };

                //worker取得進度事件
                worker.ProgressChanged += (s, ea) =>
                {
                    var number = ea.UserState.ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

                    if (number.Length == 2 && (!string.IsNullOrEmpty(number[0]) || !string.IsNullOrEmpty(number[1])))
                    {
                        try
                        {
                            var h = Convert.ToDouble(number[0]);
                            var t = Convert.ToDouble(number[1]);
                            if (h > 100 || t > 100)
                                return;
                            seriesH.Points.Add(h, count);
                            seriesT.Points.Add(t, count);
                            totalH += h;
                            totalT += t;
                            count++;
                            var xView = chart1.ChartAreas[0].AxisX.ScaleView;
                            xView.Position += (xView.Position + 30 < xView.ViewMaximum ? 30 : -xView.Position);
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine(ex.StackTrace);
                        }
                    }
                };

                //worker完成工作事件
                worker.RunWorkerCompleted += (s, ea) =>
                {
                    if (ea.Error != null)
                    {
                        MessageBox.Show("接收不到您的資料");
                    }
                    else if (ea.Cancelled)
                    {
                        totalH /= Convert.ToDouble(count);
                        totalT /= Convert.ToDouble(count);
                    }
                    else
                    {
                        totalH /= Convert.ToDouble(count);
                        totalT /= Convert.ToDouble(count);
                    }
                };

                //滑鼠點擊事件
                buttonStart.MouseClick += (s, ea) =>
                {
                    if (!comPort.IsOpen)
                    {
                        try
                        {
                            totalH = 0.0d;
                            totalT = 0.0d;
                            count = 0;
                            chart1.ChartAreas[0].AxisX.ScaleView.Position = 0;
                            seriesH.Points.Clear();
                            seriesT.Points.Clear();
                            comPort.Open();
                            worker.RunWorkerAsync();
                            buttonStart.Text = "停止";
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            MessageBox.Show("您的Arduino正在與其它程式做連接", "警告", MessageBoxButtons.OK);
                            Console.WriteLine(ex.StackTrace);
                        }
                        catch (IOException ex)
                        {
                            MessageBox.Show("無法與你的Arduino連接", "警告", MessageBoxButtons.OK);
                            Console.WriteLine(ex.StackTrace);
                        }
                    }
                    else
                    {
                        comPort.Close();
                        worker.CancelAsync();
                        buttonStart.Text = "啟動";
                    }
                };

                //禁止程式多開
                bool flag;
                this._mutex = new Mutex(true, "RDHT", out flag);
                if (!flag)
                {
                    this._mutex.Close();
                    this._mutex = null;
                    this.Close();
                }

                //設定線段序列顏色
                seriesH.Color = System.Drawing.Color.Blue;
                seriesT.Color = System.Drawing.Color.Red;

                //線段字體顏色
                seriesH.Font = new System.Drawing.Font("標楷體", 16);
                seriesT.Font = new System.Drawing.Font("標楷體", 16);

                //線段種類
                seriesH.ChartType = SeriesChartType.Line;
                seriesT.ChartType = SeriesChartType.Line;

                //線段形狀
                seriesH.MarkerStyle = MarkerStyle.Cross;
                seriesT.MarkerStyle = MarkerStyle.Square;

                //線段形狀顏色
                seriesH.MarkerColor = System.Drawing.Color.OrangeRed;
                seriesT.MarkerColor = System.Drawing.Color.LawnGreen;

                //形狀大小
                seriesH.MarkerSize = 10;
                seriesT.MarkerSize = 10;

                //線段粗度
                seriesH.BorderWidth = 5;
                seriesT.BorderWidth = 5;

                //ChartAreas
                var areas = chart1.ChartAreas[0];

                //讓卷軸自動捲動
                areas.CursorX.AutoScroll = true;
                areas.AxisX.ScaleView.Zoomable = false;
                areas.AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll;
                

                //可視範圍
                areas.AxisX.ScaleView.Zoom(0, 30);

                //外框線條粗度
                areas.AxisX.LineWidth = 2;
                areas.AxisY.LineWidth = 2;

                //Y上限
                areas.AxisY.Maximum = 100;

                //Y下限
                areas.AxisY.Minimum = 0;

                //X網格間距
                areas.AxisX.MajorGrid.Interval = 5;

                //Y網格間距
                areas.AxisY.MajorGrid.Interval = 10;

                //將線段清單加入到圖內
                chart1.Series.Add(seriesH);
                chart1.Series.Add(seriesT);

                //標題
                chart1.Titles.Add("溫溼度監測");

                //顯示使用者所選取線段數值
                chart1.GetToolTipText += (s, ea) =>
                {
                    switch (ea.HitTestResult.ChartElementType)
                    {
                        case ChartElementType.DataPoint:
                            var dataPoint = ea.HitTestResult.Series.Points[ea.HitTestResult.PointIndex];
                            var str = ea.HitTestResult.Series.Name.Equals("濕度") ? "濕度0.00'%'" : "溫度0.00°C";
                            ea.Text = dataPoint.YValues[0].ToString(str, CultureInfo.InvariantCulture);
                            break;
                    }
                };
            }
        }

    }
}
