#include <dht11.h>

#define PIN 2

dht11 DHT;

// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(9600);
}

// the loop function runs over and over again until power down or reset
void loop() {
	int status = DHT.read(PIN);
	if (status == 0) {
		Serial.print((double)DHT.humidity, 2);
		Serial.print(",");
		Serial.println((double)DHT.temperature, 2);
		delay(1000);
	}
}
